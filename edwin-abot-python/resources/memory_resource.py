"""
Contains the ram bound resource
"""
import json
import re

import falcon


class MemoryResource:
    """
    Has a method for handling ram bound get requests
    """
    def on_get(self, req: falcon.Request, resp: falcon.Response):
        """Handles GET requests"""

        with open('support/ram_test.txt', 'r') as myfile:
            test_str = myfile.read()

        regex = r"[aeiouAEIOU]"

        matches = re.findall(regex, test_str)

        resp.status = falcon.HTTP_OK
        resp.media = json.dumps({'n_vowels': len(matches)})
